import './App.css';
import Links from './components/links'
import Costumer from './pages/costumer'
import Company from './pages/company'

import { Switch, Route } from 'react-router-dom';

function App() {

  return (
    <div className="App">
      <header className="App-header">
        <Switch>
          <Route exact path="/customer/:id">
            <Costumer />
          </Route>
          <Route exact path="/company/:id">
            <Company />
          </Route>
          <Route exact path="/">
            <Links />
          </Route>
        </Switch>
      </header>
    </div>
  );
}

export default App;
