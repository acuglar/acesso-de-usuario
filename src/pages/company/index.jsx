import React from "react";
import { members } from "../../components/members.jsx";
import { useParams, Link } from "react-router-dom";

const Customer = () => {
  const params = useParams();
  const member = members.find((member) => member.id === params.id);
  console.log(member);

  return (
    <div>
      <h1>Detalhes da Empresa</h1>

      <p>Nome da empresa: {member && member.name}</p>

      <Link to="/">Voltar</Link>
    </div>
  );
};

export default Customer;
