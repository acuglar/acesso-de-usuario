import React from "react";
import { members } from "../../components/members.jsx";
import { useParams, Link } from "react-router-dom";

const Customer = () => {
  const { id } = useParams();
  const member = members.find((member) => member.id === id);
  console.log(member);

  return (
    <div>
      <h1>Detalhes do cliente</h1>

      <p>Nome: {member && member.name}</p>

      <Link to="/">Voltar</Link>
    </div>
  );
};

export default Customer;
