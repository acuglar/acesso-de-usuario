import { members } from "../members.jsx";
import { Link } from "react-router-dom";

const Links = () => {
  return (
    <div>
      {members.map((member) => {
        return member.type === "pj" ? (
          <div>
            <Link to={`/company/${member.id}`}>{member.name}</Link>
          </div>
        ) : (
          <div>
            <Link to={`/customer/${member.id}`}>{member.name}</Link>
          </div>
        );
      })}
    </div>
  );
};

export default Links;
